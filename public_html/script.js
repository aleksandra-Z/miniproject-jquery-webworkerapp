$(document).ready(function(){

  var worker = new Worker("task.js");
  
  worker.onmessage = function(e){
      if(e.data == "end")
          $("#startCountdown").removeAttr("disabled");
      else
        $("#countdown").html(e.data);
  };
     
  $("#startCountdown").click(function(){
      $(this).attr("disabled","true");
      var number = $("#number").val();
      worker.postMessage({
         'message':'start',
         'number': number
     }); 
  });
});