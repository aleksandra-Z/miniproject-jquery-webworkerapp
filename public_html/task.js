function countdown(number){
    postMessage(number--);
    if(number<0){
        postMessage("end");
        return;
    }
    setTimeout("countdown("+number+")",1000);
}


self.onmessage=function(e){
  switch(e.data.message){
      case 'start':
        countdown(e.data.number);
        break
      default:
          alert("Unknown command");
  }
};
